package com.wecode95.kaitemp.model

import android.arch.persistence.room.*

@Entity(
        tableName = "authorized_phones",
        indices = arrayOf(Index("device_phone"), Index("phone")),
        foreignKeys = arrayOf(
                ForeignKey(entity = Device::class,
                        parentColumns = arrayOf("phone"),
                        childColumns = arrayOf("device_phone"),
                        onDelete = ForeignKey.CASCADE,
                        onUpdate = ForeignKey.CASCADE)
        )
)

data class AuthorizedUser(
        @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id: Long,
        @ColumnInfo(name = "device_phone") val devicePhone: String ="",
        @ColumnInfo(name = "phone") val phone: String = ""
)
