package com.wecode95.kaitemp.model

import android.arch.persistence.room.*
import com.wecode95.kaitemp.util.SmsUtil
import timber.log.Timber
import java.util.*

@Entity(
        tableName = "alarm",
        indices = arrayOf(Index("id"), Index("device_phone")),
        foreignKeys = arrayOf(
                ForeignKey(entity = Device::class,
                        parentColumns = arrayOf("phone"),
                        childColumns = arrayOf("device_phone"),
                        onDelete = ForeignKey.CASCADE,
                        onUpdate = ForeignKey.CASCADE)
        )
)

data class Alarm(
        @ColumnInfo(name = "device_phone") var devicePhone: String,
        @ColumnInfo(name = "tag") var tag: Int,
        @ColumnInfo(name = "type") var type: Int,
        @ColumnInfo(name = "name") var name: String,
        @ColumnInfo(name = "value") var value: String = "0",
        @ColumnInfo(name = "minTemperature") var minTemperature: Int = -55,
        @ColumnInfo(name = "maxTemperature") var maxTemperature: Int = 99,
        @ColumnInfo(name = "last_checked") var last_checked: Date,
        @PrimaryKey @ColumnInfo(name = "id") var id: String = "$devicePhone$tag"
) {
//    fun shouldAlert(): Boolean {
//        var range = minTemperature..maxTemperature
//        return value.toFloat() in range
//    }

    companion object {
        fun getIntRange(r: String): Int {
//            Timber.d(r);
            if (r.isEmpty()) return 0
            if (r.contains("+") || !r.contains("-")) return r.replace("+", "").toInt()
            else return r.replace("-", "").toInt() * (-1)
        }

        fun getStringRange(r: Int): String {
//            Timber.d("$r")
            if (r >= 0) {
                if (r < 10) return "+0$r"
                else return "+$r"
            } else {
                if (r > -10) return "-0${r * -1}"
                else return r.toString()
            }
        }
    }

    fun type(): TYPE {
        return if (type == 1) TYPE.ALARM else TYPE.VOLTAGE
    }

    override fun equals(other: Any?): Boolean {
        return super.equals(other)
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }

    override fun toString(): String {
        return """
            device_phone:   $devicePhone,
            tag:            $tag,
            type:           $type,
            name:           $name,
            value:          $value,
            minTemperature: $minTemperature,
            maxTemperature: $maxTemperature,
            last_checked:   ${SmsUtil.getStringDate(last_checked)}
            id:             $id,
        """
    }

    enum class TYPE {
        ALARM, VOLTAGE
    }
}
