package com.wecode95.kaitemp.model

data class SmsReceived(
        var phone: String = "",
        var message: String = "",
        var commandType: String = ""
)
