package com.wecode95.kaitemp.model

import android.arch.persistence.room.*

@Entity(
        tableName = "device",
        indices = arrayOf(Index("phone"), Index("name"))
)

data class Device(
        @PrimaryKey @ColumnInfo(name = "phone") var phone: String = "",
        @ColumnInfo(name = "name") var name: String = "",
        @ColumnInfo(name = "password") var password: String = "",

        @Ignore
        var authorizedPhones: Array<AuthorizedUser> = emptyArray<AuthorizedUser>(),

        @Ignore
        var alarms: Array<Alarm> = emptyArray<Alarm>()
) {
    override fun equals(other: Any?): Boolean {
        return super.equals(other)
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }

    override fun toString(): String {
        return """
            name: $name
            phone: $phone
            password: $password
        """
    }
}
