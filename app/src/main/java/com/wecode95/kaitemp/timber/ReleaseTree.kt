package com.wecode95.kaitemp.timber

import android.util.Log
import timber.log.Timber


class ReleaseTree : Timber.Tree() {
    override fun isLoggable(tag: String?, priority: Int): Boolean {
        return !(priority == Log.DEBUG || priority == Log.VERBOSE || priority == Log.INFO)
    }

    override fun log(priority: Int, tag: String?, message: String?, t: Throwable?) {
        if (isLoggable(tag, priority)) {
            Log.println(priority, tag, message)
        }
    }
}