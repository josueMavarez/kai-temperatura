package com.wecode95.kaitemp.util

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.telephony.SmsMessage
import com.wecode95.kaitemp.DeviceCommand
import com.wecode95.kaitemp.model.SmsReceived
import java.text.SimpleDateFormat
import java.util.*


class SmsUtil {
    companion object {

        fun getSmsReceived(intent: Intent?): SmsReceived {
            val bundle = intent?.extras
            val sms = SmsReceived()

            if (bundle != null) {
                var pdus = bundle.get("pdus") as? Array<Any>
                pdus.let {
                    pdus!!.iterator().forEach {
                        var smsMessage = getIncomingMessage(it, bundle)
                        sms.phone = smsMessage.displayOriginatingAddress
                        sms.message = smsMessage.messageBody
                        sms.commandType = smsMessage.messageBody.substring(smsMessage.messageBody.length - 2, smsMessage.messageBody.length)
                    }
                }
            }

            return sms
        }

        @Suppress("DEPRECATION")
        private fun getIncomingMessage(`object`: Any, bundle: Bundle): SmsMessage {
            val smsMessage: SmsMessage
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val format = bundle.getString("format")
                smsMessage = SmsMessage.createFromPdu(`object` as ByteArray, format)
            } else {
                smsMessage = SmsMessage.createFromPdu(`object` as ByteArray)
            }
            return smsMessage
        }

        fun getStringDate(date: Date): String {
            return SimpleDateFormat("d/MM h:mm a", Locale.getDefault()).format(date)
        }

        fun getMessageType(sms: SmsReceived): String {
            if (sms.commandType == DeviceCommand.RTU_INFO && sms.message.length == 76) {
                return DeviceCommand.RTU_INFO
            } else if (sms.commandType == DeviceCommand.PHONES_INFO && sms.message.length == 74) {
                return DeviceCommand.PHONES_INFO
            } else if (sms.commandType == DeviceCommand.ALARMS_INFO && sms.message.length == 58) {
                return DeviceCommand.ALARMS_INFO
            } else if (sms.commandType == DeviceCommand.NEW_PASSWORD_INFO && sms.message.length == 34) {
                return DeviceCommand.NEW_PASSWORD_INFO
            } else if (sms.commandType == DeviceCommand.ALARMS_RANGE_INFO && sms.message.length == 32) {
                return DeviceCommand.ALARMS_RANGE_INFO
            } else if (sms.commandType == DeviceCommand.AUTO_MONITOR_INFO && sms.message.length == 19) {
                return DeviceCommand.AUTO_MONITOR_INFO
            } else if (sms.commandType == DeviceCommand.TEMP_01 && sms.message.length == 29) {
                return DeviceCommand.TEMP_01
            } else if (sms.commandType == DeviceCommand.TEMP_02 && sms.message.length == 29) {
                return DeviceCommand.TEMP_02
            } else if (sms.commandType == DeviceCommand.TEMP_03 && sms.message.length == 29) {
                return DeviceCommand.TEMP_03
            } else if (sms.commandType == DeviceCommand.VOLTAGE_INFO && sms.message.length == 31) {
                return DeviceCommand.VOLTAGE_INFO
            }

            return ""
        }
    }
}
