package com.wecode95.kaitemp

import android.app.Application
import com.wecode95.kaitemp.di.components.DaggerInjector
import com.wecode95.kaitemp.di.modules.DataModule
import com.wecode95.kaitemp.di.components.Injector
import com.wecode95.kaitemp.di.modules.AndroidModule
import timber.log.Timber
import com.crashlytics.android.Crashlytics
import com.wecode95.kaitemp.timber.DebugTree
import com.wecode95.kaitemp.timber.ReleaseTree
import io.fabric.sdk.android.Fabric


class KaiApplication : Application() {

    companion object {
        lateinit var injector: Injector
    }

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        } else {
            Timber.plant(ReleaseTree())
        }

        injector = DaggerInjector.builder()
                .androidModule(AndroidModule(this))
                .dataModule(DataModule())
                .build()

        Fabric.with(this, Crashlytics())
    }
}

