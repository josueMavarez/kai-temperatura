package com.wecode95.kaitemp

class DeviceCommand {

    companion object {
        val DEFAULT_PASS = "12345"

        /* Receiving Message Types */
        val RTU_INFO = "#1"
        val PHONES_INFO: String = "#2"
        val ALARMS_INFO: String = "#3"
        val NEW_PASSWORD_INFO: String = "#4"
        val ALARMS_RANGE_INFO: String = "#5"
        val AUTO_MONITOR_INFO: String = "#6"
        val VOLTAGE_INFO: String = "#a"
        val TEMP_01: String = "#e"
        val TEMP_02: String = "#f"
        val TEMP_03: String = "#g"

        /* Sending Commands types */
        val REQUEST_SIGNALS_STATUS = "*ve"
        val REQUEST_AUTH_PHONES = "*vt"
        val REQUEST_NAMES = "*vn"
        val REQUEST_RANGES = "*vr"

        val REQUEST_AUTO_MONITOR: String = "*comd"
        val REQUEST_RTU_NAME_CHANGE: String = "*cnd0"
        val REQUEST_ALARM_NAME_CHANGE: String = "*cnt"
        val REQUEST_ALARM_RANGE_CHANGE: String = "*cr"
        val REQUEST_PHONE_CHANGE: String = "*ct"
        val REQUEST_PHONE_SELF_DESTRUCT: String = "*ctsd"
        val REQUEST_PASSWORD_CHANGE: String = "*cc"

        fun changeDeviceName(name: String): String {
            return "$REQUEST_RTU_NAME_CHANGE$name"
        }

        fun changeAlarmName(index: Int, name: String): String {
            return "$REQUEST_ALARM_NAME_CHANGE$index$name"
        }

        fun changeAlarmRange(index: Int, min: String, max: String): String {
            return "$REQUEST_ALARM_RANGE_CHANGE$index$min$max"
        }

        fun addPhoneWithPass(phone: String, password: String): String {
            return "#$password$REQUEST_PHONE_CHANGE+$phone"
        }

        fun addPhone(phone: String): String {
            return "#$REQUEST_PHONE_CHANGE+$phone"
        }

        fun removePhone(phone: String): String {
            return "#$REQUEST_PHONE_CHANGE-$phone"
        }

        fun changePassword(newPassword: String): String {
            return "$REQUEST_PASSWORD_CHANGE$newPassword"
        }

        fun toggleDailyNotification(): String {
            return REQUEST_AUTO_MONITOR
        }
    }

    interface CommandListener {
        fun onCommandExecuting(commandType: String?, message: String?);

        fun onCommandExecuted(phone: String?, commandType: String?, message: String?)

        fun onCommandFailed(phone: String?, commandType: String?, message: String?)

        fun onCommandOnPause()
    }
}
