package com.wecode95.kaitemp

import android.content.Context
import android.content.Intent
import com.afollestad.materialdialogs.MaterialDialog
import com.wecode95.kaitemp.data.AppDatabase
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class CommandListenerImpl(val context: Context, val db: AppDatabase) : DeviceCommand.CommandListener {

    private var progressDialog: MaterialDialog = MaterialDialog.Builder(context)
            .content(context.getString(R.string.please_wait))
            .progress(true, 0)
            .build()

    override fun onCommandExecuting(commandType: String?, message: String?) {
        Timber.i("onCommandExecuting Message: $message, Type: $commandType")
        progressDialog.show()
    }

    override fun onCommandExecuted(phone: String?, commandType: String?, message: String?) {
        Timber.i("onCommandExecuted Message: $message, Type: $commandType")
        if (progressDialog.isShowing) progressDialog.dismiss()

        if (commandType == DeviceCommand.REQUEST_PHONE_SELF_DESTRUCT) {
            if (phone != null) {
                Observable.fromCallable { db.deviceModel().device(phone) }
                        .subscribeOn(Schedulers.computation())
                        .subscribe {
                            db.deviceModel().delete(it!!)

                            var intent = Intent("notify_update")
                            intent.putExtra("update", true)

                            context.sendBroadcast(intent)
                        }
            }
        }
    }

    override fun onCommandFailed(phone: String?, commandType: String?, message: String?) {
        Timber.e("onCommandFailed Message: $message, Type: $commandType")
        if (progressDialog.isShowing) progressDialog.dismiss()
    }

    override fun onCommandOnPause() {
        Timber.d("onCommandOnPause")
        if (progressDialog.isShowing) progressDialog.dismiss()
    }
}

