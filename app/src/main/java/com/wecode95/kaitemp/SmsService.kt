package com.wecode95.kaitemp

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.wecode95.kaitemp.data.AppDatabase
import com.wecode95.kaitemp.model.Alarm
import com.wecode95.kaitemp.model.AuthorizedUser
import com.wecode95.kaitemp.model.Device
import com.wecode95.kaitemp.util.SmsUtil
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.*
import java.util.concurrent.TimeUnit


data class Optional<out M>(val value: M?)

class SmsService : BroadcastReceiver() {

    fun notifyUpdate(context: Context) {
        Timber.d("Notify MainActivity!")

        var intent = Intent("notify_update")
        intent.putExtra("update", true)

        context.sendBroadcast(intent)
    }

    override fun onReceive(context: Context, intent: Intent) {
        val db: AppDatabase = AppDatabase.createPersistentDatabase(context)
        val sms = SmsUtil.getSmsReceived(intent)
        Timber.d("Phone: %s Message: %s Length: %s CommandType: %s", sms.phone, sms.message, sms.message.length, sms.commandType)

        Observable.defer { Observable.just(Optional(db.deviceModel().device(sms.phone))) }
                .subscribeOn(Schedulers.computation())
                .subscribe { (device) ->
                    if (device != null) {
                        Timber.d("Device %s found", device.phone);
                        when (SmsUtil.getMessageType(sms)) {
                            DeviceCommand.RTU_INFO -> {
                                Timber.d(DeviceCommand.RTU_INFO)
                                val data = arrayOf(
                                        arrayOf("${device.phone}1", sms.message.substring(22..25)),
                                        arrayOf("${device.phone}2", sms.message.substring(37..40)),
                                        arrayOf("${device.phone}3", sms.message.substring(52..55)),
                                        arrayOf("${device.phone}4", sms.message.substring(67..73))
                                )

                                data.map {
                                    db.alarmModel().alarm(it[0])?.let { alarm ->
                                        if (alarm.value != it[1]) {
                                            alarm.value = it[1]
                                            alarm.last_checked = Date()
                                            db.alarmModel().update(alarm)
                                        }
                                    }
                                }
                                // Notify alarm statuses
                            }
                            DeviceCommand.PHONES_INFO -> {
                                Timber.d(DeviceCommand.PHONES_INFO)
                                val phones = arrayOf(
                                        sms.message.substring(11..22),
                                        sms.message.substring(23..34),
                                        sms.message.substring(35..46),
                                        sms.message.substring(47..58),
                                        sms.message.substring(59..70)
                                )

                                db.authorizedUserModel().deleteByPhone(device.phone)

                                phones.map { AuthorizedUser(id = 0, devicePhone = device.phone, phone = it) }
                                        .forEach {
                                            if (!it.phone.contains("*"))
                                                db.authorizedUserModel().createOrUpdate(it)
                                        }
                            }
                            DeviceCommand.ALARMS_INFO -> {
                                Timber.d(DeviceCommand.ALARMS_INFO)
                                val alarms = arrayOf(
                                        sms.message.substring(11..21).trim(),
                                        sms.message.substring(22..32).trim(),
                                        sms.message.substring(33..43).trim(),
                                        sms.message.substring(44..54).trim()
                                )

                                device.name = sms.message.substring(0..10).trim()
                                db.deviceModel().update(device)

                                alarms.mapIndexed { i, a ->
                                    db.alarmModel().alarm("${device.phone}${i + 1}")?.let { alarm ->
                                        Timber.d("Alarm ${alarm.id} found")
                                        if (alarm.name != a) {
                                            alarm.name = a
                                            db.alarmModel().update(alarm)
                                        }
                                    }
                                }
                            }
                            DeviceCommand.NEW_PASSWORD_INFO -> {
                                Timber.d(DeviceCommand.NEW_PASSWORD_INFO)
                                val newPassword = sms.message.substring(26..31)
                                device.password = newPassword
                                db.deviceModel().update(device)
                                // Notify!
                            }
                            DeviceCommand.ALARMS_RANGE_INFO -> {
                                Timber.d(DeviceCommand.ALARMS_RANGE_INFO)
                                val ranges = arrayOf(
                                        arrayOf(sms.message.substring(11, 14), sms.message.substring(14, 17)),
                                        arrayOf(sms.message.substring(17, 20), sms.message.substring(20, 23)),
                                        arrayOf(sms.message.substring(23, 26), sms.message.substring(26, 29))
                                )

                                ranges.mapIndexed { i, r ->
                                    Timber.d("$i ${r[0]} ${r[1]} ${device.phone}${i + 1}")
                                    db.alarmModel().alarm("${device.phone}${i + 1}")?.let { alarm ->
                                        Timber.d("Alarm ${alarm.id} found")
                                        if (alarm.type() == Alarm.TYPE.ALARM) {
                                            val min = Alarm.getIntRange(r[0])
                                            val max = Alarm.getIntRange(r[1])
                                            alarm.minTemperature = min
                                            alarm.maxTemperature = max
                                            alarm.last_checked = Date()
                                            Timber.d("min $min max $max")
                                        }
                                        db.alarmModel().update(alarm)
                                        // Notify!
                                    }
                                }
                            }
                            DeviceCommand.AUTO_MONITOR_INFO -> {
                                Timber.d(DeviceCommand.AUTO_MONITOR_INFO)
                                val dailyStatus = sms.message.substring(2..5)
                            }
                            DeviceCommand.TEMP_01 -> {
                                Timber.d(DeviceCommand.TEMP_01)
                                db.alarmModel().alarm("${device.phone}1")?.let { alarm ->
                                    val value = sms.message.substring(22..25)
                                    alarm.value = value
                                    alarm.last_checked = Date()
                                    db.alarmModel().update(alarm)
                                    // Notify
                                }
                            }
                            DeviceCommand.TEMP_02 -> {
                                Timber.d(DeviceCommand.TEMP_02)
                                db.alarmModel().alarm("${device.phone}2")?.let { alarm ->
                                    val value = sms.message.substring(22..25)
                                    alarm.value = value
                                    alarm.last_checked = Date()
                                    db.alarmModel().update(alarm)
                                    // Notify
                                }
                            }
                            DeviceCommand.TEMP_03 -> {
                                Timber.d(DeviceCommand.TEMP_03)
                                db.alarmModel().alarm("${device.phone}3")?.let { alarm ->
                                    val value = sms.message.substring(22..25)
                                    alarm.value = value
                                    alarm.last_checked = Date()
                                    db.alarmModel().update(alarm)
                                    // Notify
                                }
                            }
                            DeviceCommand.VOLTAGE_INFO -> {
                                Timber.d(DeviceCommand.VOLTAGE_INFO)
                                db.alarmModel().alarm("${device.phone}4")?.let { alarm ->
                                    val value = sms.message.substring(22..28)
                                    alarm.value = value
                                    alarm.last_checked = Date()
                                    db.alarmModel().update(alarm)
                                    // Notify
                                }
                            }
                        }
                    } else {
                        if (SmsUtil.getMessageType(sms) == DeviceCommand.PHONES_INFO) {
                            Timber.d("Device %s not found", sms.phone);
                            val newDevice = Device(phone = sms.phone, name = sms.message.substring(0..10).trim(), password = "12345")
                            db.deviceModel().createOrUpdate(newDevice).let {
                                val phones = arrayOf(
                                        sms.message.substring(11..22),
                                        sms.message.substring(23..34),
                                        sms.message.substring(35..46),
                                        sms.message.substring(47..58),
                                        sms.message.substring(59..70)
                                )

                                phones.map { AuthorizedUser(id = 0, devicePhone = newDevice.phone, phone = it) }
                                        .forEach { db.authorizedUserModel().createOrUpdate(it) }

                                (1..3).map { Alarm(devicePhone = newDevice.phone, tag = it, name = "TEMP $it", type = 1, last_checked = Date()) }
                                        .forEach {
                                            db.alarmModel().createOrUpdate(it)
                                            Timber.d("Alarm ${it.id} created")
                                        }

                                val voltage = Alarm(devicePhone = newDevice.phone, tag = 4, name = "VOLTAGE", type = 0, value = "NORMAL", last_checked = Date())
                                db.alarmModel().createOrUpdate(voltage)

                                val commandManager = CommandManager(context.applicationContext, CommandListenerImpl(context, db))

                                Completable.timer(5, TimeUnit.SECONDS)
                                        .subscribe { commandManager.executeCommand(newDevice.phone, DeviceCommand.REQUEST_NAMES, DeviceCommand.REQUEST_NAMES) }

                                Completable.timer(10, TimeUnit.SECONDS)
                                        .subscribe { commandManager.executeCommand(newDevice.phone, DeviceCommand.REQUEST_RANGES, DeviceCommand.REQUEST_RANGES) }

                                Completable.timer(15, TimeUnit.SECONDS)
                                        .subscribe { commandManager.executeCommand(newDevice.phone, DeviceCommand.REQUEST_SIGNALS_STATUS, DeviceCommand.REQUEST_SIGNALS_STATUS) }
                            }
                        }
                    }

                    notifyUpdate(context)
                }
    }
}


