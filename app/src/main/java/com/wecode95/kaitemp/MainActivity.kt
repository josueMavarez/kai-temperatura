package com.wecode95.kaitemp

import android.Manifest
import android.arch.lifecycle.ViewModelProviders
import android.content.*
import android.os.Build
import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.design.widget.TabLayout
import android.support.v13.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.Toolbar
import android.text.InputType
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.FrameLayout
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.afollestad.materialdialogs.MaterialDialog
import com.github.rubensousa.bottomsheetbuilder.BottomSheetBuilder
import com.wecode95.kaitemp.data.AppDatabase
import com.wecode95.kaitemp.model.AuthorizedUser
import com.wecode95.kaitemp.model.Device
import com.wecode95.kaitemp.ui.AlarmViewModel
import com.wecode95.kaitemp.ui.DeviceViewModel
import com.wecode95.kaitemp.ui.adapter.DevicePagerAdapter
import com.wecode95.kaitemp.util.plus
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @BindView(R.id.mainView) lateinit var mainView: CoordinatorLayout
    @BindView(R.id.toolbar) lateinit var toolbar: Toolbar
    @BindView(R.id.config) lateinit var configFab: FloatingActionButton
    @BindView(R.id.refresh) lateinit var refresh: AppCompatButton
    @BindView(R.id.emptyView) lateinit var emptyView: FrameLayout
    @BindView(R.id.container) lateinit var viewPager: ViewPager
    @BindView(R.id.tabs) lateinit var tabs: TabLayout

    private val devicePagerAdapter = DevicePagerAdapter(supportFragmentManager)
    private lateinit var commandListenerImpl: CommandListenerImpl

    private lateinit var deviceViewModel: DeviceViewModel
    private lateinit var alarmViewModel: AlarmViewModel

    private var disposables = CompositeDisposable()
    private lateinit var commandManager: CommandManager
    private var updateReceiver: BroadcastReceiver? = null

    @Inject lateinit var db: AppDatabase
    @Inject lateinit var pref: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)
        KaiApplication.injector.inject(this@MainActivity)
        setSupportActionBar(toolbar)

        if (supportActionBar != null) {
            supportActionBar!!.title = "KAI TEMPERATURA"
        }

        deviceViewModel = ViewModelProviders.of(this@MainActivity).get(DeviceViewModel::class.java)
        alarmViewModel = ViewModelProviders.of(this@MainActivity).get(AlarmViewModel::class.java)
        commandListenerImpl = CommandListenerImpl(this@MainActivity, db)
        commandManager = CommandManager(this@MainActivity, commandListenerImpl)

        tabs.setupWithViewPager(viewPager)
        viewPager.adapter = devicePagerAdapter

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this@MainActivity, arrayOf<String>(Manifest.permission.SEND_SMS, Manifest.permission.RECEIVE_SMS), 1)
        }
    }

    fun getCurrentDevice(): Device {
        Timber.d("Current Item ${viewPager.currentItem}")
        Timber.d("Current Device ${devicePagerAdapter.getDevice(viewPager.currentItem)}")
        return devicePagerAdapter.getDevice(viewPager.currentItem)
    }

    @OnClick(R.id.refresh)
    fun refresh() {
        val device = getCurrentDevice()
        commandManager.executeCommand(device.phone, DeviceCommand.REQUEST_SIGNALS_STATUS, DeviceCommand.REQUEST_SIGNALS_STATUS)
    }

    @OnClick(R.id.config)
    fun config() {
        val device = getCurrentDevice()

        BottomSheetBuilder(this@MainActivity)
                .setMode(BottomSheetBuilder.MODE_LIST)
                .setMenu(R.menu.menu_device)
                .setItemClickListener {
                    when (it.itemId) {
                        R.id.changeName -> {
                            MaterialDialog.Builder(this@MainActivity)
                                    .title(R.string.change_name)
                                    .content(R.string.phone)
                                    .inputType(InputType.TYPE_CLASS_TEXT)
                                    .input("KAITEMPERATURA", device.name, { _, _ -> })
                                    .inputRange(10, 10)
                                    .onPositive({ dialog, _ ->
                                        val newKaiName = dialog.inputEditText!!.text.toString()
                                        commandManager.executeCommand(device.phone,
                                                DeviceCommand.changeDeviceName(newKaiName),
                                                DeviceCommand.REQUEST_RTU_NAME_CHANGE)
                                    })
                                    .show()
                        }
                        R.id.changePass -> {
                            MaterialDialog.Builder(this@MainActivity)
                                    .title(R.string.change_pass)
                                    .content(R.string.phone)
                                    .inputType(InputType.TYPE_CLASS_TEXT)
                                    .input("", device.password, { _, _ -> })
                                    .inputRange(5, 5)
                                    .onPositive({ dialog, _ ->
                                        val newPassword = dialog.inputEditText!!.text.toString()
                                        commandManager.executeCommand(device.phone,
                                                DeviceCommand.changePassword(newPassword),
                                                DeviceCommand.REQUEST_PASSWORD_CHANGE)
                                    })
                                    .show()
                        }
                        R.id.addNumber -> {
                            Observable.fromCallable { deviceViewModel.getAuthorizedPhones(device.phone) }
                                    .subscribeOn(Schedulers.computation())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe { authorizedPhones ->
                                        if (authorizedPhones.size < 5) {
                                            MaterialDialog.Builder(this@MainActivity)
                                                    .title(R.string.add_new_phone)
                                                    .content(R.string.phone)
                                                    .inputType(InputType.TYPE_CLASS_TEXT)
                                                    .input("04121234567", "", { _, _ -> })
                                                    .inputRange(11, 11)
                                                    .onPositive({ dialog, _ ->
                                                        val newPhone = dialog.inputEditText!!.text.toString()
                                                        commandManager.executeCommand(device.phone,
                                                                DeviceCommand.addPhone(newPhone),
                                                                DeviceCommand.REQUEST_PHONE_CHANGE)
                                                    })
                                                    .show()
                                        } else {
                                            Snackbar.make(mainView, "Numero maximo de telefonos alcanzado", Snackbar.LENGTH_LONG).show()
                                        }
                                    }
                        }
                        R.id.deleteNumber -> {
                            Observable.fromCallable { deviceViewModel.getAuthorizedPhones(device.phone) }
                                    .subscribeOn(Schedulers.computation())
                                    .map { convertListToString(it) }
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe {
                                        MaterialDialog.Builder(this@MainActivity)
                                                .title(R.string.delete_number)
                                                .autoDismiss(false)
                                                .items(it)
                                                .itemsCallback { _, _, position, _ ->
                                                    MaterialDialog.Builder(this@MainActivity)
                                                            .title("Confirmar")
                                                            .content("Esta seguro de elimar el numero '${it[position]}'")
                                                            .positiveText("OK")
                                                            .onPositive({ _, _ ->
                                                                commandManager.executeCommand(device.phone,
                                                                        DeviceCommand.removePhone(it[position]),
                                                                        DeviceCommand.REQUEST_PHONE_CHANGE)
                                                            })
                                                            .show()
                                                }
                                                .show()
                                    }
                        }
                        R.id.toggleDailyStatus -> {
                            commandManager.executeCommand(device.phone, DeviceCommand.REQUEST_AUTO_MONITOR, DeviceCommand.toggleDailyNotification())
                        }
                        R.id.deleteKai -> {
                            val phone = pref.getString("phone", "")
                            commandManager.executeCommand(device.phone, DeviceCommand.removePhone(phone), DeviceCommand.REQUEST_PHONE_SELF_DESTRUCT)
                        }
                    }
                }
                .setIconTintColor(ContextCompat.getColor(this@MainActivity, R.color.colorSecondaryText))
                .createDialog()
                .show()
    }

    @OnClick(R.id.add)
    fun add() {
        addNewDevice()
    }

    private fun convertListToString(list: List<AuthorizedUser>): List<String> {
        var newList = arrayListOf<String>()
        list.forEach { if (!it.phone.contains("*")) newList.add(it.phone) }
        return newList
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        when (id) {
        // TODO: Remove in production
            R.id.action_settings -> {
            }
            R.id.editPhone -> {
                val phone = pref.getString("phone", "")

                MaterialDialog.Builder(this@MainActivity)
                        .title(R.string.edit_phone)
                        .content(getString(R.string.phone))
                        .inputType(InputType.TYPE_CLASS_PHONE)
                        .input("04121234567", phone, { _, _ -> })
                        .inputRange(11, 11)
                        .onPositive({ dialog, _ ->
                            val localPhone = dialog.inputEditText!!.text.toString()
                            pref.edit().putString("phone", localPhone).commit()
                        })
                        .show()
            }
            R.id.addDevice -> {
                addNewDevice()
            }
            R.id.about -> {

            }
        }

        return super.onOptionsItemSelected(item)
    }

    fun addNewDevice() {
        val phone = pref.getString("phone", null)
        Timber.d(phone)
        phone?.let {
            MaterialDialog.Builder(this@MainActivity)
                    .title(R.string.add_kai)
                    .content(R.string.phone)
                    .inputType(InputType.TYPE_CLASS_PHONE)
                    .input("04121234567", "", { _, _ -> })
                    .inputRange(11, 11)
                    .onPositive({ dialog, _ ->
                        val devicePhone = dialog.inputEditText!!.text.toString()
                        commandManager.executeCommand(devicePhone,
                                DeviceCommand.addPhoneWithPass(phone, DeviceCommand.DEFAULT_PASS),
                                DeviceCommand.REQUEST_PHONE_CHANGE)
                    })
                    .show()
        } ?: Toast.makeText(this, getString(R.string.must_add_phone), Toast.LENGTH_SHORT).show();
    }

    fun updateUiDevices() {
        disposables += deviceViewModel.getDevices()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { devices ->
                    // Update ViewPager.data
                    devicePagerAdapter.bind(devices)

                    // Update Views.visibility
                    emptyView.visibility = if (devices.size > 0) View.GONE else View.VISIBLE
                    tabs.visibility = if (devices.size > 0) View.VISIBLE else View.GONE
                    refresh.visibility = if (devices.size > 0) View.VISIBLE else View.GONE
                    configFab.visibility = if (devices.size > 0) View.VISIBLE else View.GONE
                }
    }

    override fun onStart() {
        super.onStart()
        updateUiDevices()

        if (updateReceiver == null) {
            updateReceiver = object : BroadcastReceiver() {
                override fun onReceive(context: Context?, intent: Intent) {
                    var update = intent?.extras.getBoolean("update")
                    if (update) updateUiDevices()
                }
            }
        }

        registerReceiver(updateReceiver, IntentFilter("notify_update"))
    }

    override fun onStop() {
        super.onStop()
        commandListenerImpl.onCommandOnPause()
        if (disposables == null) return
        disposables.clear()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (updateReceiver != null) unregisterReceiver(updateReceiver)
    }
}
