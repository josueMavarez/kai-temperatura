package com.wecode95.kaitemp.di.components

import com.wecode95.kaitemp.di.modules.AndroidModule
import com.wecode95.kaitemp.MainActivity
import com.wecode95.kaitemp.SmsService
import com.wecode95.kaitemp.di.modules.DataModule
import com.wecode95.kaitemp.ui.AlarmViewModel
import com.wecode95.kaitemp.ui.DeviceViewModel
import com.wecode95.kaitemp.ui.KaiFragment
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(DataModule::class, AndroidModule::class))
interface Injector {
    fun inject(into: DeviceViewModel)
    fun inject(into: AlarmViewModel)
    fun inject(into: MainActivity)
    fun inject(into: KaiFragment)
    fun inject(into: SmsService)
}