package com.wecode95.kaitemp.di.modules

import android.content.Context
import android.preference.PreferenceManager
import com.wecode95.kaitemp.data.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataModule {
    @Provides
    @Singleton
    fun provideAppDatabase(context: Context) = AppDatabase.createPersistentDatabase(context)

    @Provides
    fun providesSharedPreferences(context: Context) = PreferenceManager.getDefaultSharedPreferences(context)
}