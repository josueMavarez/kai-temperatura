package com.wecode95.kaitemp.data

import android.arch.persistence.room.*
import com.wecode95.kaitemp.model.Device
import io.reactivex.Flowable
import timber.log.Timber

@Dao
abstract class DeviceDao {

    @Query("SELECT COUNT(*) FROM device")
    abstract fun count(): Int

    @Query("SELECT * FROM device")
    abstract fun devices(): Flowable<List<Device>>

    @Query("SELECT * FROM device WHERE phone = :p0")
    abstract fun device(phone: String): Device?

    fun createOrUpdate(device: Device) {
        Timber.d("Device to create $device")
        insertOrUpdate(device)
    }

    fun createTestDevice(name: String, phone: String) {
        val device: Device = Device(phone, name, "12345")

        Timber.d("Device to create $device")
        insertOrUpdate(device)
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertOrUpdate(vararg devices: Device)

    @Delete
    abstract fun delete(device: Device)

    @Update
    abstract fun update(device: Device)
}
