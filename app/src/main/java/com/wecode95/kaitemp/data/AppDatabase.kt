package com.wecode95.kaitemp.data

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context
import com.wecode95.kaitemp.model.Alarm
import com.wecode95.kaitemp.model.AuthorizedUser
import com.wecode95.kaitemp.model.Device
import com.wecode95.kaitemp.ui.DateConverter

@Database(
        entities = arrayOf(Device::class, Alarm::class, AuthorizedUser::class),
        version = 1
)
@TypeConverters(DateConverter::class)

abstract class AppDatabase : RoomDatabase() {
    abstract fun deviceModel(): DeviceDao
    abstract fun alarmModel(): AlarmDao
    abstract fun authorizedUserModel(): AuthorizedUserDao

    companion object {
        private const val DB_NAME = "kaixxxxx.db"

        fun createInMemoryDatabase(context: Context): AppDatabase
                = Room.inMemoryDatabaseBuilder(context.applicationContext, AppDatabase::class.java)
                .build()

        fun createPersistentDatabase(context: Context): AppDatabase
                = Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, DB_NAME)
                .build()
    }
}
