package com.wecode95.kaitemp.data

import android.arch.persistence.room.*
import com.wecode95.kaitemp.model.AuthorizedUser
import io.reactivex.Flowable

@Dao
abstract class AuthorizedPhoneDao {

    @Query("SELECT * FROM authorized_phones WHERE device_phone = :p0")
    abstract fun authorizedPhonesRtu(phone: String): Flowable<List<AuthorizedUser>>

    fun createOrUpdate(phone: AuthorizedUser) {
        insertOrUpdate(phone)
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertOrUpdate(vararg phones: AuthorizedUser)

    @Delete
    abstract fun delete(vararg phone: AuthorizedUser);
}

