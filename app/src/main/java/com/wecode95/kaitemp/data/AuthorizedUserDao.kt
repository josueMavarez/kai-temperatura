package com.wecode95.kaitemp.data

import android.arch.persistence.room.*
import com.wecode95.kaitemp.model.Alarm
import com.wecode95.kaitemp.model.AuthorizedUser
import io.reactivex.Flowable

@Dao
abstract class AuthorizedUserDao {

    @Query("SELECT * FROM authorized_phones WHERE device_phone = :p0")
    abstract fun authorizedPhones(phone: String): List<AuthorizedUser>

    @Query("SELECT * FROM authorized_phones WHERE device_phone = :p0 AND phone = :p1")
    abstract fun authorizedPhone(devicePhone: String, phone: String): AuthorizedUser?

    @Query("DELETE FROM authorized_phones WHERE device_phone = :p0")
    abstract fun deleteByPhone(devicePhone: String)

    fun createOrUpdate(authorizedPhone: AuthorizedUser) {
        insertOrUpdate(authorizedPhone)
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertOrUpdate(vararg authorizedPhones: AuthorizedUser)

    @Delete
    abstract fun delete(vararg authorizedPhone: AuthorizedUser)

    @Update
    abstract fun update(authorizedPhone: AuthorizedUser)
}
