package com.wecode95.kaitemp.data

import android.arch.persistence.room.*
import com.wecode95.kaitemp.model.Alarm
import io.reactivex.Flowable

@Dao
abstract class AlarmDao {

    @Query("SELECT * FROM alarm WHERE id = :p0")
    abstract fun alarm(id: String): Alarm

    @Query("SELECT * FROM alarm WHERE device_phone = :p0")
    abstract fun alarmsRtu(phone: String): Flowable<List<Alarm>>

    @Query("SELECT * FROM alarm WHERE device_phone = :p0")
    abstract fun alarms(phone: String): List<Alarm>

    fun createOrUpdate(alarm: Alarm) {
        insertOrUpdate(alarm)
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertOrUpdate(vararg alarms: Alarm)

    @Delete
    abstract fun delete(vararg alarm: Alarm)

    @Update
    abstract fun update(alarm: Alarm)
}
