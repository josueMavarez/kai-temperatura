package com.wecode95.kaitemp

import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.telephony.SmsManager
import timber.log.Timber

class CommandManager(private val context: Context, private val commandListener: DeviceCommand.CommandListener?) {
    val SENT: String = "SMS_SENT"
    val RECEIVED = "android.provider.Telephony.SMS_RECEIVED"
    val SMS_OK = -1
    val debugCommandMessage = "debugCommandMessage"
    val debugCommandType = "debugCommandType"
    val phoneDevice = "devicePhone"

    fun executeCommand(phone: String, command: String, commandType: String) {
        var sendPack: Bundle = Bundle()
        sendPack.putString(debugCommandMessage, "Command: $command, Type: $commandType, Device: $phone")
        sendPack.putString(debugCommandType, commandType)
        sendPack.putString(phoneDevice, phone)

        var intentSend: Intent = Intent(SENT)
        intentSend.putExtras(sendPack)

        var intentReceive: Intent = Intent(RECEIVED)
        intentReceive.putExtras(sendPack)

        var pendingIntentSend: PendingIntent = PendingIntent.getBroadcast(context, 0, intentSend, PendingIntent.FLAG_UPDATE_CURRENT)
        var pendingIntentReceived: PendingIntent = PendingIntent.getBroadcast(context, 0, intentReceive, PendingIntent.FLAG_UPDATE_CURRENT)
        context.registerReceiver(getSendBroadcast(), IntentFilter(SENT))

        val smsManager: SmsManager = SmsManager.getDefault()
        smsManager.sendTextMessage(phone, null, command, pendingIntentSend, pendingIntentReceived)

        commandListener?.onCommandExecuting(commandType, "Command: $command, Type: $commandType, Device: $phone")
        Timber.d("Sending command... phone: $phone, message: $command, type: $commandType")
    }

    fun getSendBroadcast(): BroadcastReceiver {
        return object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent?) {
                val debugCommandMessage = intent?.extras?.getString(debugCommandMessage)
                val debugCommandType = intent?.extras?.getString(debugCommandType)
                val phone = intent?.extras?.getString(phoneDevice)

                when (resultCode) {
                    SMS_OK -> {
                        commandListener?.onCommandExecuted(phone, debugCommandType, debugCommandMessage)
                    }
                    SmsManager.RESULT_ERROR_GENERIC_FAILURE -> {
                        commandListener?.onCommandFailed(phone, debugCommandType, context?.getString(R.string.sms_error))
                    }
                    SmsManager.RESULT_ERROR_RADIO_OFF -> {
                        commandListener?.onCommandFailed(phone, debugCommandType, context?.getString(R.string.sms_error_radio_off))
                    }
                    SmsManager.RESULT_ERROR_NULL_PDU -> {
                        commandListener?.onCommandFailed(phone, debugCommandType, context?.getString(R.string.sms_error_pdu_null))
                    }
                    SmsManager.RESULT_ERROR_NO_SERVICE -> {
                        commandListener?.onCommandFailed(phone,debugCommandType, context?.getString(R.string.sms_error_no_service))
                    }
                }

                context.unregisterReceiver(this)
            }
        }
    }

}
