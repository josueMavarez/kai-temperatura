package com.wecode95.kaitemp

import dagger.Component

@Component(
        modules = arrayOf()
)

interface SmsServiceComponent {
    fun inject(into: SmsService)
}