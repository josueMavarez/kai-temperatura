package com.wecode95.kaitemp.ui.adapter

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.wecode95.kaitemp.R
import com.wecode95.kaitemp.model.Alarm
import com.wecode95.kaitemp.ui.CustomCircularProgressView
import com.wecode95.kaitemp.util.SmsUtil
import timber.log.Timber

internal class AlarmViewHolder(context: Context, parent: ViewGroup)
    : RecyclerView.ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_alarm, parent, false)) {

    @BindView(R.id.AlarmDrawable) lateinit var drawable: ImageView
    @BindView(R.id.AlarmName) lateinit var name: TextView
    @BindView(R.id.AlarmLastChecked) lateinit var lastChecked: TextView
    @BindView(R.id.temp) lateinit var temperature: CustomCircularProgressView
    @BindView(R.id.tempText) lateinit var temperatureText: TextView

    init {
        ButterKnife.bind(this, itemView)
    }

    fun bind(alarm: Alarm) {
//        Timber.d("Binder alarm ${alarm.toString()}")

        drawable.setImageResource(R.drawable.ic_thermometer)
        name.text = alarm.name
        lastChecked.text = SmsUtil.getStringDate(alarm.last_checked)
        temperatureText.text = "${alarm.value}º"
        temperature.maxProgress = 99f

        if (alarm.value.trim().isNotEmpty()) {
            temperature.progress = alarm.value.toFloat()
        }

        temperature.progressColor = Color.parseColor("#64DD17")
    }
}
