package com.wecode95.kaitemp.ui.adapter

import android.annotation.SuppressLint
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.PagerAdapter
import com.wecode95.kaitemp.model.Device
import com.wecode95.kaitemp.ui.KaiFragment
import timber.log.Timber

class DevicePagerAdapter constructor(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {
    private val fragmentManager: FragmentManager = fragmentManager
    private var devices: List<Device> = emptyList()
    private var fragmentPool: ArrayList<Fragment> = ArrayList<Fragment>()

    override fun getItem(position: Int): Fragment {
        return KaiFragment.newInstance(devicePhone = devices[position].phone)
    }

    override fun getCount(): Int {
        return devices.size
    }

    override fun getPageTitle(position: Int): CharSequence {
        return devices[position].name
    }

    @SuppressLint("RestrictedApi")
    override fun getItemPosition(`object`: Any?): Int {
        var fragment: Fragment = `object` as Fragment
        fragmentPool = ArrayList(this.fragmentManager.fragments)

        if (fragmentPool.contains(fragment)) {
            return PagerAdapter.POSITION_NONE
        } else {
            return PagerAdapter.POSITION_UNCHANGED
        }
    }

    fun getDevice(position: Int): Device {
        return devices[position]
    }

    fun bind(devices: List<Device>) {
        if (devices.size > 0) {
            this.devices = emptyList()
            this.devices = devices;
            this.notifyDataSetChanged()
        }
    }
}
