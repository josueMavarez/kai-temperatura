package com.wecode95.kaitemp.ui.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.wecode95.kaitemp.R
import com.wecode95.kaitemp.model.Alarm
import com.wecode95.kaitemp.util.SmsUtil
import timber.log.Timber

internal class VoltageViewHolder(context: Context, parent: ViewGroup)
    : RecyclerView.ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_voltage, parent, false)) {

    @BindView(R.id.AlarmDrawable) lateinit var drawable: ImageView
    @BindView(R.id.AlarmName) lateinit var name: TextView
    @BindView(R.id.AlarmLastChecked) lateinit var lastChecked: TextView
    @BindView(R.id.AlarmStatus) lateinit var status: TextView

    init {
        ButterKnife.bind(this, itemView)
    }

    fun bind(alarm: Alarm) {
//        Timber.d("Binder alarm ${alarm.toString()}")

        drawable.setImageResource(R.drawable.ic_high_voltage)
        name.text = alarm.name
        status.text = alarm.value
        lastChecked.text = SmsUtil.getStringDate(alarm.last_checked)
    }
}

