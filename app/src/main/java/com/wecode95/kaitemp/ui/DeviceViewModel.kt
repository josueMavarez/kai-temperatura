package com.wecode95.kaitemp.ui

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.wecode95.kaitemp.KaiApplication
import com.wecode95.kaitemp.data.AppDatabase
import com.wecode95.kaitemp.model.Device
import timber.log.Timber
import javax.inject.Inject

class DeviceViewModel constructor(application: Application) : AndroidViewModel(application) {

    @Inject lateinit var db: AppDatabase

    init {
        KaiApplication.injector.inject(this@DeviceViewModel)
    }

    fun count() = db.deviceModel().count();

    fun getDevices() = db.deviceModel().devices();

    fun getAuthorizedPhones(phone: String) = db.authorizedUserModel().authorizedPhones(phone)

    fun getDevice(phone: String) = db.deviceModel().device(phone)

    fun create(device: Device): Device {
        db.deviceModel().createOrUpdate(device).let {
            return device
        }
    }

    fun createTestDevice(name: String, phone: String) {
        db.deviceModel().createTestDevice(name, phone)
    }

    fun delete(phone: String): Device? {
        db.deviceModel().device(phone)?.let {
            Timber.d("Found device ${it.phone}")
            db.deviceModel().delete(it)
            return it
        }

        return null
    }
}
