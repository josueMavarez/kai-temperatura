package com.wecode95.kaitemp.ui

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.os.Bundle
import android.support.v13.view.ViewCompat
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout

class CustomCircularProgressView(context: Context, attrs: AttributeSet) : FrameLayout(context, attrs) {

    private var mInnerView: InnerView? = null
    private var mMaxProgress: Float = 0.toFloat()
    private var mProgress: Float = 0.toFloat()
    private var mProgressView: ProgressView? = null
    private var mThicknessRatio: Float = 0.toFloat()
    private var mTrackView: TrackView? = null

    init {
        this.mMaxProgress = 100.0f
        this.mProgress = 0.0f
        this.mThicknessRatio = 0.175f
        init()
    }

    private fun init() {
        this.mTrackView = TrackView(context)
        this.mTrackView!!.layoutParams = FrameLayout.LayoutParams(-1, -1)
        this.mInnerView = InnerView(context)
        this.mInnerView!!.layoutParams = FrameLayout.LayoutParams(-1, -1)
        this.mProgressView = ProgressView(context)
        this.mProgressView!!.layoutParams = FrameLayout.LayoutParams(-1, -1)
        addView(this.mTrackView)
        addView(this.mProgressView)
        addView(this.mInnerView)
    }

    fun setProperties(properties: Bundle) {
        val progress = properties.getFloat(PROGRESS, -1.0f)
        val progressColor = properties.getInt(PROGRESS_COLOR, -1)
        val trackColor = properties.getInt(TRACK_COLOR, -1)
        val innerColor = properties.getInt(INNER_COLOR, -1)
        val thicknessRatio = properties.getFloat(THICKNESS_RATIO, -1.0f)
        if (progress != -1.0f) {
            this.mProgress = progress
        }
        if (thicknessRatio != -1.0f) {
            this.mThicknessRatio = thicknessRatio
        }
        if (progressColor != -1) {
            this.mProgressView!!.setProgressColor(progressColor, false)
        }
        if (trackColor != -1) {
            this.mTrackView!!.setTrackColor(trackColor, false)
        }
        if (innerColor != -1) {
            this.mInnerView!!.setInnerColor(innerColor, false)
        }
        this.mProgressView!!.invalidate()
        this.mTrackView!!.invalidate()
        this.mInnerView!!.invalidate()
    }

    var maxProgress: Float
        get() = this.mMaxProgress
        set(maxProgress) {
            this.mMaxProgress = maxProgress
            this.mProgressView!!.invalidate()
        }

    var progress: Float
        get() = this.mProgress
        set(progress) {
            var progress = progress
            if (this.mProgress > this.mMaxProgress) {
                progress = this.mMaxProgress
            }
            this.mProgress = progress
            this.mProgressView!!.invalidate()
        }

    var progressColor: Int
        get() = this.mProgressView!!.progressColor
        set(color) = this.mProgressView!!.setProgressColor(color, true)

    var innerColor: Int
        get() = this.mInnerView!!.innerColor
        set(color) = this.mInnerView!!.setInnerColor(color, true)

    var trackColor: Int
        get() = this.mTrackView!!.trackColor
        set(color) = this.mTrackView!!.setTrackColor(color, true)

    var thicknessRatio: Float
        get() = this.mThicknessRatio
        set(thicknessRatio) {
            this.mThicknessRatio = thicknessRatio
            this.mProgressView!!.invalidate()
            this.mInnerView!!.invalidate()
        }

    private inner class InnerView(context: Context) : View(context) {
        private val boundsRect: RectF = RectF()
        private val mInnerPaint: Paint = Paint(1)
        private val overlayRect: RectF = RectF()

        init {
            this.mInnerPaint.color = -1
        }

        override fun onDraw(canvas: Canvas) {
            super.onDraw(canvas)
            this.boundsRect.set(0.0f, 0.0f, canvas.width.toFloat(), canvas.height.toFloat())
            val pathWidth = this.boundsRect.width() / 2.0f * this@CustomCircularProgressView.mThicknessRatio
            this.overlayRect.set(pathWidth, pathWidth, this.boundsRect.width() - pathWidth, this.boundsRect.height() - pathWidth)
            canvas.drawArc(this.overlayRect, 0.0f, 360.0f, true, this.mInnerPaint)
        }

        fun setInnerColor(color: Int, invalidate: Boolean) {
            if (innerColor != color) {
                this.mInnerPaint.color = color
                if (invalidate) {
                    invalidate()
                }
            }
        }

        var innerColor: Int
            get() = this.mInnerPaint.color
            set(color) = setInnerColor(color, true)
    }

    private inner class ProgressView(context: Context) : View(context) {
        private val boundsRect: RectF = RectF()
        private val endEllipseRect: RectF = RectF()
        private val mProgressPaint: Paint = Paint(1)
        private val startEllipseRect: RectF = RectF()

        init {
            this.mProgressPaint.color = ViewCompat.MEASURED_STATE_MASK
        }

        override fun onDraw(canvas: Canvas) {
            super.onDraw(canvas)
            this.boundsRect.set(0.0f, 0.0f, canvas.width.toFloat(), canvas.height.toFloat())
            val angle = this@CustomCircularProgressView.mProgress / this@CustomCircularProgressView.mMaxProgress * 360.0f
            val radius = this.boundsRect.width() / 2.0f
            canvas.drawArc(this.boundsRect, 270.0f, angle, true, this.mProgressPaint)
            val radians = 3.141592653589793 * (angle.toDouble() / 180.0 - 0.5)
            val pathWidth = radius * this@CustomCircularProgressView.mThicknessRatio
            val xOffset = radius * (1.0f + (1.0f - this@CustomCircularProgressView.mThicknessRatio / 2.0f) * Math.cos(radians).toFloat())
            val yOffset = radius * (1.0f + (1.0f - this@CustomCircularProgressView.mThicknessRatio / 2.0f) * Math.sin(radians).toFloat())
            this.startEllipseRect.set(radius - pathWidth / 2.0f, 0.0f, radius - pathWidth / 2.0f + pathWidth, pathWidth)
            this.endEllipseRect.set(xOffset - pathWidth / 2.0f, yOffset - pathWidth / 2.0f, xOffset - pathWidth / 2.0f + pathWidth, yOffset - pathWidth / 2.0f + pathWidth)
            canvas.drawOval(this.startEllipseRect, this.mProgressPaint)
            canvas.drawOval(this.endEllipseRect, this.mProgressPaint)
        }

        fun setProgressColor(color: Int, invalidate: Boolean) {
            if (progressColor != color) {
                this.mProgressPaint.color = color
                if (invalidate) {
                    invalidate()
                }
            }
        }

        var progressColor: Int
            get() = this.mProgressPaint.color
            set(color) = setProgressColor(color, true)
    }

    private inner class TrackView(context: Context) : View(context) {
        private val boundsRect: RectF = RectF()
        private val mTrackPaint: Paint = Paint(1)

        init {
            this.mTrackPaint.color = Color.parseColor("#F0F0F0")
        }

        override fun onDraw(canvas: Canvas) {
            super.onDraw(canvas)
            this.boundsRect.set(0.0f, 0.0f, canvas.width.toFloat(), canvas.height.toFloat())
            canvas.drawArc(this.boundsRect, 0.0f, 360.0f, true, this.mTrackPaint)
        }

        fun setTrackColor(color: Int, invalidate: Boolean) {
            if (trackColor != color) {
                this.mTrackPaint.color = color
                if (invalidate) {
                    invalidate()
                }
            }
        }

        var trackColor: Int
            get() = this.mTrackPaint.color
            set(color) = setTrackColor(color, true)
    }

    companion object {
        var INNER_COLOR: String = "3"
        var PROGRESS: String = "0"
        var PROGRESS_COLOR: String = "1"
        var THICKNESS_RATIO: String = "4"
        var TRACK_COLOR: String = "2"
    }
}
