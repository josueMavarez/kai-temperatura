package com.wecode95.kaitemp.ui

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.wecode95.kaitemp.KaiApplication
import com.wecode95.kaitemp.data.AppDatabase
import com.wecode95.kaitemp.model.Alarm
import javax.inject.Inject

class AlarmViewModel constructor(application: Application) : AndroidViewModel(application) {

    @Inject lateinit var db : AppDatabase

    init {
        KaiApplication.injector.inject(this@AlarmViewModel)
    }

    fun getAlarmsByDevice(phone: String) = db.alarmModel().alarmsRtu(phone)

    fun create(alarm: Alarm) = db.alarmModel().createOrUpdate(alarm)

    fun delete(phone: String): Boolean? {
        db.alarmModel().alarms(phone).let {
            it.forEach {
                db.alarmModel().delete(it)
            }
            return true
        }

        return false
    }
}
