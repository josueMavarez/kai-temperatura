package com.wecode95.kaitemp.ui.adapter

import android.content.Context
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.ViewGroup
import com.wecode95.kaitemp.model.Alarm
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import timber.log.Timber

class DevicesAdapter(private val context: Context, private val listener: SimpleClick) : RecyclerView.Adapter<ViewHolder>() {

    private var alarms: List<Alarm> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        when (getType(viewType)) {
            Alarm.TYPE.ALARM -> return AlarmViewHolder(context, parent)
            Alarm.TYPE.VOLTAGE -> return VoltageViewHolder(context, parent)
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when (alarms[position].type()) {
            Alarm.TYPE.ALARM -> (holder as AlarmViewHolder).bind(alarm = alarms[position])
            Alarm.TYPE.VOLTAGE -> (holder as VoltageViewHolder).bind(alarm = alarms[position])
        }

        holder.itemView.setOnClickListener { listener.onClick(alarms[position], position) }
    }

    override fun getItemCount(): Int {
        return alarms.size
    }

    override fun getItemId(position: Int): Long {
        return alarms[position].id.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return alarms[position].type
    }

    fun bind(source: List<Alarm>) {
        this.alarms = source
        notifyDataSetChanged()
    }

    // TODO: Return Flowable<List<Alarm>> from disposable on KaiFragment
    fun bind(source: Flowable<List<Alarm>>): Disposable {
        data class DataWithDiff(val counters: List<Alarm>, val diff: DiffUtil.DiffResult?)
        return source
                .scan(DataWithDiff(emptyList(), null)) { (old), new ->
                    DataWithDiff(new, DiffUtil.calculateDiff(DiffCallback(old, new)))
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    alarms = it.counters
                    if (it.diff != null) {
                        it.diff.dispatchUpdatesTo(this)
                    } else {
                        notifyDataSetChanged()
                    }
                }
    }

    fun getType(type: Int) = if (type == 1) Alarm.TYPE.ALARM else Alarm.TYPE.VOLTAGE

    private class DiffCallback(private val old: List<Alarm>, private val new: List<Alarm>) : DiffUtil.Callback() {
        override fun getOldListSize() = old.size

        override fun getNewListSize() = new.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int)
                = old[oldItemPosition].id == new[newItemPosition].id

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int)
                = old[oldItemPosition] == new[newItemPosition]
    }

    interface SimpleClick {
        fun onClick(alarm: Alarm, position: Int)
    }

}
